$(document).ready(function () {

    // Fixed Header Positioning
    function calcHeaderPositioning() {
        $rowWidth = 1400;
        $windowWidth = $(window).width();
        $fixedPositioning = ($windowWidth - $rowWidth) / 2;

        if ($(window).width() > 1200) {
            $("header").css("right", $fixedPositioning);
        } else {
            $("header").css("right", 'auto');
        }
    }

    // Fixed header Functionality
    function isHeaderFixed() {
        $headerHeight = $("header").outerHeight();
        $currentPosition = $(window).scrollTop();

        if ($currentPosition > $headerHeight) {
            $("header").addClass("fixed-header");
        } else {
            $("header").removeClass("fixed-header");
        }
    }

    // Mobile Nav Toggle
    $(".mobile-toggle").click(function () {
        $(".mobile-toggle").toggleClass("active-burger");
        $(".mobile-nav-wrap").toggleClass("active-nav");
    });

    // Slideshow Carousel
    $('.slideshow-wrap').slick({
        accessibility: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        dots: false,
        infinite: true,
        pauseOnHover: true,
        respondTo: "window",
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 600,
        swipe: true,
        variableWidth: false,
    });

    // Twitter Carousel
    $('.twitter-wrap').slick({
        accessibility: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        dots: true,
        infinite: true,
        pauseOnHover: true,
        respondTo: "window",
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 600,
        swipe: true,
        variableWidth: false,
    });

    // Function Calls
    calcHeaderPositioning();
    isHeaderFixed();

    // Resize Window Function Calls
    $(window).resize(function () {
        calcHeaderPositioning();
    });

    // Window Scroll Function Calls
    $(window).scroll(function () {
        isHeaderFixed();
    });

});
