<nav class='desktop-nav'>
    <li class='home-link'><a href='<?php echo $project_root; ?>'><i class="fas fa-home"></i></a></li>
    <li><a href='<?php echo $project_root; ?>company'>Company</a></li>
    <li><a href='<?php echo $project_root; ?>security-services'>Security Services</a></li>
    <li><a href='<?php echo $project_root; ?>database-services'>Database Services</a></li>
    <li><a href='<?php echo $project_root; ?>resources'>Resources</a></li>
    <li><a href='<?php echo $project_root; ?>contact'>Contact</a></li>
    <li class='search'><a href='#'><i class="fas fa-search"></i></a></li>
</nav>