<div class='mobile-nav-wrap'>

    <?php include('modules/nav/burger.php'); ?>

    <nav class='mobile-nav'>
        <li><a href='<?php echo $project_root; ?>'>Home</a></li>
        <li><a href='<?php echo $project_root; ?>company'>Company</a></li>
        <li><a href='<?php echo $project_root; ?>security-services'>Security Services</a></li>
        <li><a href='<?php echo $project_root; ?>database-services'>Database Services</a></li>
        <li><a href='<?php echo $project_root; ?>resources'>Resources</a></li>
        <li><a href='<?php echo $project_root; ?>contact'>Contact</a></li>
        <li class='search'><a href='#'>Search</a></li>
    </nav>

</div>
