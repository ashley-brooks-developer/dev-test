<div class='slideshow-container'>

    <div class='slideshow-wrap'>

        <?php for($i=1; $i <=3; $i++): ?>
        <div class='slide'>
            <img src='img/banner.jpg' />
            <div class='slideshow-caption'>
                <h2>Web Application Security & Penetration Testing</h2>
            </div>
        </div>
        <?php endfor; ?>

    </div>

    <div class='slideshow-buttons'>
        <div class='button'>
            <a href='#'>Security Services <i class="fas fa-angle-right"></i></a>
        </div>
        <div class='button'>
            <a href='#'>Database Services <i class="fas fa-angle-right"></i></a>
        </div>
    </div>

</div>
