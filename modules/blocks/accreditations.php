<?php
$accreditations = array('crest-accredited.png', 'iso.png', 'tigerscheme.png', 'pci.png', 'check.png');
?>

    <div class='accreditations-container'>

        <?php foreach($accreditations as $accreditation): ?>
            <img src='img/<?php echo $accreditation; ?>' />
        <?php endforeach; ?>

    </div>
