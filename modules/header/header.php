<header>

    <div class='logo'>
        <a href='<?php echo $project_root; ?>'>
            <img src='img/logo.png' />
        </a>
    </div>

    <?php include('modules/nav/burger.php'); ?>
    
    <?php include('modules/nav/desktop-nav.php'); ?>

    <?php include('modules/nav/mobile-nav.php'); ?>

</header>
