<?php

// Header includes
include 'modules/header/header-essentials.php';
include 'modules/header/header.php';

// Slider
include 'modules/blocks/slider.php';

// Main Content Block
include 'modules/blocks/main-content.php';

// Accreditations
include 'modules/blocks/accreditations.php';

// Footer includes
include 'modules/footer/footer.php';
include 'modules/footer/footer-essentials.php';