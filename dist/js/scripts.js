$(document).ready(function () {

    // Fixed Header Positioning
    function calcHeaderPositioning() {
        $rowWidth = 1400;
        $windowWidth = $(window).width();
        $fixedPositioning = ($windowWidth - $rowWidth) / 2;

        if ($(window).width() > 1200) {
            $("header").css("right", $fixedPositioning);
        } else {
            $("header").css("right", 'auto');
        }
    }

    // Fixed header Functionality
    function isHeaderFixed() {
        $headerHeight = $("header").outerHeight();
        $currentPosition = $(window).scrollTop();

        if ($currentPosition > $headerHeight) {
            $("header").addClass("fixed-header");
        } else {
            $("header").removeClass("fixed-header");
        }
    }

    // Mobile Nav Toggle
    $(".mobile-toggle").click(function () {
        $(".mobile-toggle").toggleClass("active-burger");
        $(".mobile-nav-wrap").toggleClass("active-nav");
    });

    // Slideshow Carousel
    $('.slideshow-wrap').slick({
        accessibility: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        dots: false,
        infinite: true,
        pauseOnHover: true,
        respondTo: "window",
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 600,
        swipe: true,
        variableWidth: false,
    });

    // Twitter Carousel
    $('.twitter-wrap').slick({
        accessibility: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000,
        dots: true,
        infinite: true,
        pauseOnHover: true,
        respondTo: "window",
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 600,
        swipe: true,
        variableWidth: false,
    });

    // Function Calls
    calcHeaderPositioning();
    isHeaderFixed();

    // Resize Window Function Calls
    $(window).resize(function () {
        calcHeaderPositioning();
    });

    // Window Scroll Function Calls
    $(window).scroll(function () {
        isHeaderFixed();
    });

});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNjcmlwdC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6InNjcmlwdHMuanMiLCJzb3VyY2VzQ29udGVudCI6WyIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG5cbiAgICAvLyBGaXhlZCBIZWFkZXIgUG9zaXRpb25pbmdcbiAgICBmdW5jdGlvbiBjYWxjSGVhZGVyUG9zaXRpb25pbmcoKSB7XG4gICAgICAgICRyb3dXaWR0aCA9IDE0MDA7XG4gICAgICAgICR3aW5kb3dXaWR0aCA9ICQod2luZG93KS53aWR0aCgpO1xuICAgICAgICAkZml4ZWRQb3NpdGlvbmluZyA9ICgkd2luZG93V2lkdGggLSAkcm93V2lkdGgpIC8gMjtcblxuICAgICAgICBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPiAxMjAwKSB7XG4gICAgICAgICAgICAkKFwiaGVhZGVyXCIpLmNzcyhcInJpZ2h0XCIsICRmaXhlZFBvc2l0aW9uaW5nKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICQoXCJoZWFkZXJcIikuY3NzKFwicmlnaHRcIiwgJ2F1dG8nKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIEZpeGVkIGhlYWRlciBGdW5jdGlvbmFsaXR5XG4gICAgZnVuY3Rpb24gaXNIZWFkZXJGaXhlZCgpIHtcbiAgICAgICAgJGhlYWRlckhlaWdodCA9ICQoXCJoZWFkZXJcIikub3V0ZXJIZWlnaHQoKTtcbiAgICAgICAgJGN1cnJlbnRQb3NpdGlvbiA9ICQod2luZG93KS5zY3JvbGxUb3AoKTtcblxuICAgICAgICBpZiAoJGN1cnJlbnRQb3NpdGlvbiA+ICRoZWFkZXJIZWlnaHQpIHtcbiAgICAgICAgICAgICQoXCJoZWFkZXJcIikuYWRkQ2xhc3MoXCJmaXhlZC1oZWFkZXJcIik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAkKFwiaGVhZGVyXCIpLnJlbW92ZUNsYXNzKFwiZml4ZWQtaGVhZGVyXCIpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gTW9iaWxlIE5hdiBUb2dnbGVcbiAgICAkKFwiLm1vYmlsZS10b2dnbGVcIikuY2xpY2soZnVuY3Rpb24gKCkge1xuICAgICAgICAkKFwiLm1vYmlsZS10b2dnbGVcIikudG9nZ2xlQ2xhc3MoXCJhY3RpdmUtYnVyZ2VyXCIpO1xuICAgICAgICAkKFwiLm1vYmlsZS1uYXYtd3JhcFwiKS50b2dnbGVDbGFzcyhcImFjdGl2ZS1uYXZcIik7XG4gICAgfSk7XG5cbiAgICAvLyBTbGlkZXNob3cgQ2Fyb3VzZWxcbiAgICAkKCcuc2xpZGVzaG93LXdyYXAnKS5zbGljayh7XG4gICAgICAgIGFjY2Vzc2liaWxpdHk6IHRydWUsXG4gICAgICAgIGFycm93czogZmFsc2UsXG4gICAgICAgIGF1dG9wbGF5OiB0cnVlLFxuICAgICAgICBhdXRvcGxheVNwZWVkOiAzMDAwLFxuICAgICAgICBkb3RzOiBmYWxzZSxcbiAgICAgICAgaW5maW5pdGU6IHRydWUsXG4gICAgICAgIHBhdXNlT25Ib3ZlcjogdHJ1ZSxcbiAgICAgICAgcmVzcG9uZFRvOiBcIndpbmRvd1wiLFxuICAgICAgICBzbGlkZXNUb1Nob3c6IDEsXG4gICAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgICBzcGVlZDogNjAwLFxuICAgICAgICBzd2lwZTogdHJ1ZSxcbiAgICAgICAgdmFyaWFibGVXaWR0aDogZmFsc2UsXG4gICAgfSk7XG5cbiAgICAvLyBUd2l0dGVyIENhcm91c2VsXG4gICAgJCgnLnR3aXR0ZXItd3JhcCcpLnNsaWNrKHtcbiAgICAgICAgYWNjZXNzaWJpbGl0eTogdHJ1ZSxcbiAgICAgICAgYXJyb3dzOiBmYWxzZSxcbiAgICAgICAgYXV0b3BsYXk6IHRydWUsXG4gICAgICAgIGF1dG9wbGF5U3BlZWQ6IDMwMDAsXG4gICAgICAgIGRvdHM6IHRydWUsXG4gICAgICAgIGluZmluaXRlOiB0cnVlLFxuICAgICAgICBwYXVzZU9uSG92ZXI6IHRydWUsXG4gICAgICAgIHJlc3BvbmRUbzogXCJ3aW5kb3dcIixcbiAgICAgICAgc2xpZGVzVG9TaG93OiAxLFxuICAgICAgICBzbGlkZXNUb1Njcm9sbDogMSxcbiAgICAgICAgc3BlZWQ6IDYwMCxcbiAgICAgICAgc3dpcGU6IHRydWUsXG4gICAgICAgIHZhcmlhYmxlV2lkdGg6IGZhbHNlLFxuICAgIH0pO1xuXG4gICAgLy8gRnVuY3Rpb24gQ2FsbHNcbiAgICBjYWxjSGVhZGVyUG9zaXRpb25pbmcoKTtcbiAgICBpc0hlYWRlckZpeGVkKCk7XG5cbiAgICAvLyBSZXNpemUgV2luZG93IEZ1bmN0aW9uIENhbGxzXG4gICAgJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIGNhbGNIZWFkZXJQb3NpdGlvbmluZygpO1xuICAgIH0pO1xuXG4gICAgLy8gV2luZG93IFNjcm9sbCBGdW5jdGlvbiBDYWxsc1xuICAgICQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24gKCkge1xuICAgICAgICBpc0hlYWRlckZpeGVkKCk7XG4gICAgfSk7XG5cbn0pO1xuIl19
